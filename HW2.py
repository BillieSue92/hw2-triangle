# ---------------------------------------------------- #
# File: HW2.py
# ---------------------------------------------------- #
# Author(s): Crystal Liu, Ivy McGinley (BillieSue92)
# ---------------------------------------------------- #
# Plaftorm:    Windows
# Environment: Python 2.7.7 :Anaconda 2.0.1
# Libaries:    numpy 1.9.0
#              matplotlib 1.4.0
#       	   scipy 0.14.0
#			   cv2 2.4.10
# ---------------------------------------------------- #
# Description: This program only partially completes 
# parts 1 - 3 of Homework 2
# ---------------------------------------------------- #


#----------------------------------------------------------------#

#						IMPORTED MODULES                         #

#----------------------------------------------------------------#

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
import cv2

#----------------------------------------------------------------#

#							FUNCTIONS                            #

#----------------------------------------------------------------#

### Binary Corner Detection
def get_binary_vertex(ImageName):
	image = misc.imread(ImageName).astype(np.float)
	close = ndimage.binary_closing(image)
	open = ndimage.binary_opening(image)
	return ndimage.binary_dilation(open - close, structure = np.ones((5,5)))

def disp_binary_corner(image_original, image_binary):
	plt.figure(1)
	plt.subplot(121)
	plt.imshow(image_original)
	plt.title('Original Image')
	plt.subplot(122)
	plt.imshow(image_binary)
	plt.title('Binary Corner Detection')
	plt.show()

### Harris Corner Detection
def Harris(ImageName):
	image = cv2.imread(ImageName).astype(np.float32)
	gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
	Harry = cv2.cornerHarris(gray,2,3,0.2)
	Harry = cv2.dilate(Harry,None)
	image[Harry>0.01*Harry.max()] = [0, 0, 255]
	cv2.imshow('Harris Corner Detection', image)
	cv2.waitKey(0)



#----------------------------------------------------------------#

#					PART 1: DATA GENERATION                      #

#----------------------------------------------------------------#

class triangle_image:
	def __init__(self):
		#creates an array of empty pixels
		self.iHeight = 250
		self.iWidth = self.iHeight

		# self.row = [(0)]*self.iWidth
		# self.array = [self.row]*self.iHeight
		self.array = np.zeros((self.iWidth , self.iHeight))

		#creates a list of coordinates: [(1x,1y) , (2x,2y) , (3x,3y)]
		self.vertices = [(self.iWidth/2, rnd.randint(self.iHeight/2, self.iHeight - 1)), (rnd.randint(0, self.iWidth/2), rnd.randint(0, self.iHeight/2)), (rnd.randint(self.iWidth/2, self.iWidth - 1), rnd.randint(0, self.iHeight/2))]
		self.origin = [self.iWidth/2, self.iHeight/20]

	def getVertices(self):
		return self.vertices

	def rotate(self, theta = 30):
	# rotates vertices of triangle counter-clockwise in degrees
		# x' = xcos(theta) - ysin(theta)
		# y' = ycos(theta) + xsin(theta)

		# print "Theta2: " , theta
		temp = [(0)]*3

		for ii in range (0 , 3):
			x0 = self.vertices[ii][0]- self.origin[0]
			
			y0 = self.vertices[ii][1] - self.origin[1]
			# print "\npoint:", ii , "\noriginal - 8     x0:" , x0 , "\t\ty0:" , y0

			x = x0*np.cos(theta) - y0*np.sin(theta) + self.origin[0]
			
			y = y0*np.cos(theta) + x0*np.sin(theta) + self.origin[1]
			# print "calculated       x1:" , x , "\ty1:" , y
			# x = x + self.origin[0]
			
			# y = y + self.origin[0]
			# print "rounded          xF:" , x , "\t\tyF:" , y

			temp[ii] = (x,y)

		# print "final:" , temp
		self.vertices = temp

		return self.vertices
		
	def calcDist(self):
	#calculates distance of points from origin

		dist = [0]*3

		for ii in range (0 , 3):
			x = self.vertices[ii][0]- self.origin[0]
			y = self.vertices[ii][1] - self.origin[1]

			d = np.sqrt(x**2 + y**2)
			dist[ii] = np.rint(d)

		return dist

	def getArray(self):
		return self.array

	def fillArray(self):
	#fills the array with pixels and returns the new array
		#reset array
		self.array = np.zeros((self.iWidth , self.iHeight))

		#slope from point A to point B
		if (self.vertices[0][0] - self.vertices[1][0]) == 0:
			mAB = 1000000
		else:
			mAB = (self.vertices[0][1] - self.vertices[1][1])*1.0/((self.vertices[0][0] - self.vertices[1][0]))
		#slope from point C to point B
		if (self.vertices[2][0] - self.vertices[1][0]) == 0:
			mCB = 1000000
		else:
			mCB = (self.vertices[2][1] - self.vertices[1][1])*1.0/(self.vertices[2][0] - self.vertices[1][0])
		#slope from point A to point C
		if (self.vertices[0][0] - self.vertices[2][0]) == 0:
			mAC = -1000000
		else:
			mAC = (self.vertices[0][1] - self.vertices[2][1])*1.0/(self.vertices[0][0] - self.vertices[2][0])
		
		# print self.vertices[0][1] - self.vertices[1][1], "/" , self.vertices[0][0] - self.vertices[1][0] 
		# print "mAB:" , mAB
		# print self.vertices[2][1] - self.vertices[1][1], "/" , self.vertices[2][0] - self.vertices[1][0]
		# print "mCB:" , mCB
		# print self.vertices[0][1] - self.vertices[2][1] , "/" , self.vertices[0][0] - self.vertices[2][0]
		# print "mAC:" , mAC

		#find min and max range to fill in triangles
		xmin = np.int(min(self.array[0][0], self.array[1][0], self.array[2][0], 0))
		xmax = np.int(max(self.array[0][0], self.array[1][0], self.array[2][0], self.iWidth-1))
		ymin = np.int(min(self.array[0][1], self.array[1][1], self.array[2][1], 0))
		ymax = np.int(max(self.array[0][1], self.array[1][1], self.array[2][1], self.iHeight-1))

		for xx in range (xmin, xmax):
			# arrayRow = [0]*self.iWidth
			for yy in range (ymin, ymax):
				mPB = 0
				mPC = 0

				#slope from point P to B
				if (xx - self.vertices[1][0]) == 0:
					mPB = 1000000
				else:
					mPB = (yy - self.vertices[1][1])*1.0/(xx - self.vertices[1][0])
				
				#slope from point P to C
				if (xx - self.vertices[2][0]) == 0:
					mPC = -1000000	
				else:
					mPC = (yy - self.vertices[2][1])*1.0/(xx - self.vertices[2][0])

				ABmax = max(mAB, mCB)
				ABmin = min(mAB, mCB)

				ACmax = max(mAC, mCB)
				ACmin = min(mAC, mCB)

				if ABmax>=mPB and mPB>=ABmin and ACmax>=mPC and mPC>=ACmin:
					self.array[xx][yy] = np.float(1)
			# self.array[xx] = arrayRow

		 

		return self.array

	def drawImage(self, index,  sigma = 0, fileName = "triangle"):
		im = ndimage.gaussian_filter(self.array, sigma)
		name = '%s_%03d.png' % (fileName, index)
		misc.imsave(name, im)

#tester code
A = triangle_image()

for i in range(0, 3):
	A.fillArray()
	A.getVertices()
	A.rotate(5)

	A.drawImage(i)


#----------------------------------------------------------------#

#					PART 2: CORNER DETECTION                     #

#----------------------------------------------------------------#

### Showing that binary corner detection works
image = misc.imread("triangle_000.png").astype(np.float) #######
new = get_binary_vertex("triangle_000.png") 				######

disp_binary_corner(image,new)

### Showing that Harris corner detection works
new2 = Harris('triangle_000.png')

#----------------------------------------------------------------#

#					PART 3: CORNER TRACKING                      #

#----------------------------------------------------------------#

# #Obtain video for optical flow
# vid = cv2.VideoCapture("Triangle.mp4")

# ###OPTICAL FLOW USING LUCAS-KANADE

# #Lucas-Kanade parameters
# lk_params = dict( winSize  = (15,15),
#               maxLevel = 2,
#               criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

# #Corner detection of first frame (using Harris corner detection)
# ret, old_frame = vid.read()
# old_frame = cv2.cvtColor(old_frame,cv2.COLOR_BGR2GRAY)
# Ho = Harris(old_frame)


