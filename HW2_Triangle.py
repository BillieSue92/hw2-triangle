#------------------------------------------------#
# File: HW2_Triangle.py
#------------------------------------------------#
# Authors: Crystal Liu, Ivy McGinley (BillieSue92)
#------------------------------------------------#
# Description: Randomly creates a triangle

import numpy as np
import numpy.random as rnd
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
import glob as glob

#------------------------------------------------#
class triangle_image:
	def __init__(self):
		#creates an array of empty pixels
		self.iHeight = 20
		self.iWidth = 20

		self.row = [(0)]*self.iWidth
		self.array = [self.row]*self.iHeight
		self.arrayEmpty = 1

		#creates a list of coordinates: [(1x,1y) , (2x,2y) , (3x,3y)]
		self.vertices = [(self.iWidth/2, rnd.randint(self.iHeight/2, self.iHeight)), (rnd.randint(0, self.iWidth/2), rnd.randint(0, self.iHeight/2)), (rnd.randint(self.iWidth/2, self.iWidth), rnd.randint(0, self.iHeight/2))]
		#sets origin at the center of the frame
		self.origin = (self.iHeight/2,self.iWidth/2)

	def getVertices(self):
		return self.vertices

	def rotate(self, theta):
		# rotates vertices of triangle counter clockwise in degrees
		# x' = xcos(theta) - ysin(theta)
		# y' = ycos(theta) + xsin(theta)

		theta = theta*np.pi/180
		temp = [(0)]*3

		for ii in range (0 , 3):
			x0 = self.vertices[ii][0]- self.origin[0]
			y0 = self.vertices[ii][1] - self.origin[1]

			x = np.rint(x0*np.cos(theta) - y0*np.sin(theta))
			y = np.rint(y0*np.cos(theta) + x0*np.sin(theta))

			x = x + self.origin[0]
			y = y + self.origin[0]

			temp[ii] = (x,y)

		self.vertices = temp

		return self.vertices
		
	
	def calcDist(self):
		#calculates distance of points from origin

		dist = [0]*3

		for ii in range (0 , 3):
			x = self.vertices[ii][0]- self.origin[0]
			y = self.vertices[ii][1] - self.origin[1]

			d = np.sqrt(x**2 + y**2)
			dist[ii] = np.rint(d)

		return dist

	def getArray(self):
		return self.array

	def arrayEmpty(self):
		return self.arrayEmpty

	def fillArray(self):
		#fills the empty array with pixels

		#slope from point A to point B
		if (self.vertices[0][0] - self.vertices[1][0]) == 0:
			mAB = 1000000
		else:
			mAB = (self.vertices[0][1] - self.vertices[1][1])*1.0/((self.vertices[0][0] - self.vertices[1][0]))
		#slope from point C to point B
		if (self.vertices[2][0] - self.vertices[1][0]) == 0:
			mCB = 1000000
		else:
			mCB = (self.vertices[2][1] - self.vertices[1][1])*1.0/(self.vertices[2][0] - self.vertices[1][0])
		#slope from point A to point C
		if (self.vertices[0][0] - self.vertices[2][0]) == 0:
			mAC = -1000000
		else:
			mAC = (self.vertices[0][1] - self.vertices[2][1])*1.0/(self.vertices[0][0] - self.vertices[2][0])
		
		print self.vertices[0][1] - self.vertices[1][1], "/" , self.vertices[0][0] - self.vertices[1][0] 
		print "mAB:" , mAB
		print self.vertices[2][1] - self.vertices[1][1], "/" , self.vertices[2][0] - self.vertices[1][0]
		print "mCB:" , mCB
		print self.vertices[0][1] - self.vertices[2][1] , "/" , self.vertices[0][0] - self.vertices[2][0]
		print "mAC:" , mAC

		for xx in range (0, self.iHeight):
			arrayRow = [0]*self.iWidth
			for yy in range (0, self.iWidth):
				mPB = 0
				mPC = 0

				if (xx - self.vertices[1][0]) == 0:
					mPB = 1000000

				elif (xx - self.vertices[2][0]) == 0:
					mPC = -1000000
					
				else:
					#slope from point P to B
					mPB = (yy - self.vertices[1][1])*1.0/(xx - self.vertices[1][0])
					#slope from point P to C
					mPC = (yy - self.vertices[2][1])*1.0/(xx - self.vertices[2][0])

				print "\n(" , xx , "," , yy , ")" , "mPB:" , mPB , "mPC" , mPC

				if mAB>=mPB:
					print "mAB>=mPB"
				if mPB>=mCB:
					print "mPB>=mCB"
				if mCB>=mPC:
					print "mCB>=mPC"
				if mPC>=mAC:
					print"mPC>=mAC"

				if mAB>=mPB and mPB>=mCB and mCB>=mPC and mPC>=mAC:
					arrayRow[yy] = 1
			self.array[xx] = arrayRow
		
		# #mark vertices
		# for ii in range (0, 3):
		# 	x = self.vertices[ii][0]
		# 	y = self.vertices[ii][1]
		# 	self.array[x][y] = 9

		self.arrayEmpty = 0

		return self.array

#tester code
A = triangle_image()
print A
plt.show(A)

